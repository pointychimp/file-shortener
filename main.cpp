#include <iostream>
#include <vector>
#include <string>
#include <fstream>

bool find(std::string needle, std::vector<std::string> haystack)
{
    for (unsigned int i = 0; i < haystack.size(); i++)
    {
        if (needle == haystack.at(i)) return true;
    }
    return false;
}

int main(int argc, char *argv[])
{
    std::fstream oldFile, newFile;
    std::string oldName, newName;
    std::vector<std::string> words;
    bool sameName = false;

    if (argc < 4)
    {
        std::cout << "Example usage:\n";
        std::cout << "FileShortener --old oldfile.txt --new newfile.txt\n";
        std::cout << "FileShortener -s -o oldfile.txt\n\n";
        std::cout << "--old -o     specify the file to be read from\n";
        std::cout << "--new -n     specify the file to write to\n";
        std::cout << "--same -s    when used, overwrite old file, ignored if --new used\n\n";
        std::cout << "This program reads in words in the given old file and \n";
        std::cout << "then writes only unique words in the given new file.\n\n";
        return 1;
    }

    for (int i = 0; i + 1 < argc; i++)
    {
        static std::vector<std::string> args(argv, argv+argc);
        if (args.at(i) == "--old" || args.at(i) == "-o")
        {
            oldName = args.at(i+1);
            continue;
        }
        if (args.at(i) == "--n" || args.at(i) == "-n")
        {
            newName = args.at(i+1);
            continue;
        }
        if (args.at(i) == "--same" || args.at(i) == "-s")
        {
            sameName = true;
            continue;
        }
    }
    oldFile.open(oldName.c_str(), std::ios::in);
    if (!oldFile.is_open())
    {
        std::cout << "Couldn't open old file\n";
        return 1;
    }
    if (sameName && newName == "")
    {
        newName = oldName;
    }
    while (!oldFile.eof())
    {
        static std::string temp;
        oldFile >> temp;
        if (!find(temp, words))
        {
            words.push_back(temp);
        }
    }
    oldFile.close();
    newFile.open(newName.c_str(), std::ios::out | std::ios::trunc);
    if (!newFile.is_open())
    {
        std::cout << "Couldn't open new file\n";
        return 1;
    }
    for (unsigned int i = 0; i < words.size(); i++)
    {
        newFile << words.at(i) << "\n";
    }
    newFile.close();

    return 0;
}
